<?php

require __DIR__ . '/../autoloader.php';

use PurePhpApi\Exception;
use PurePhpApi\Dispatcher;
use PurePhpApi\DependencyInjectionContainer;

try {

    $container = new DependencyInjectionContainer($_SERVER, $_GET, $_POST);
    $dispatcher = new Dispatcher($container);
    $command = $dispatcher->dispatch($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
    $response = $command->execute();
} catch (Exception\NoMatchingRoute $e) {

    $statusMsg = $e->getMessage() ?: 'WAT?';
    $statusCode = $e->getCode();

} catch (\Exception $e) {

    $statusMsg = $e->getMessage() ?: 'No soup for you!';
    $statusCode = $e->getCode() ?: 500;

} finally {

    http_response_code($response["status"] ?: $statusCode);
    header('Content-Type: application/json');
    echo json_encode($response["body"]);
}
