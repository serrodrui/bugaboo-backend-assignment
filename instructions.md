# Bugaboo Backend Assignment 

Your assignemnt is to create a small API for Translations. Even though it is not real code, please treat it as if it would be deployed to thousands of users.

- Translations are spread across the files in the `resources` directory. 
- Every file represents a combination of resource type, language and country. 
- Filename has the following pattern: `{resource}_{language}_{country}.properties`. 
- Language and country are represented by ISO 3166-1 alpha-2 codes.
- Authentication is not required. All endpoints can be open endpoints. 
- Unique identifier can be constrcuted from a combination of `section` and `key` values.

Your job is to:
- Build an api that serves data provided in `resources` directory. 
- Implement endpoints listed below. 
- Implement filtering based on language, section and key. 

The business rules are as follows:
- Translation is defined with key, resource, language and translation value. 
- Translation key is unique within a resource.

# Endpoints 

The API needs to support the following endpoints:

* [Get Translations](#get-all-translations) : `GET /api/translations/`
* [Add Translation](#add-translation) : `POST /api/translations/`
* [Update Translation](#update-translation) : `PUT /api/translations/{id}`
* [Delete Translation](#delete-translation) : `DELETE /api/translations/{id}`

The API needs to support filtering based on language, section and key. You are free to decide how to implement filtering. 

## Get Translations 

**URL** : `/api/translations/`

**Method** : `GET`

### Success Responses

**Condition** : User can get all Translations.

**Code** : `200 OK`

```json
[
    {
        "key": "account.page.title.myaccount",
        "language": "default",
        "resource": "account",
        "translation": "My Account"
    },
    {
        "key": "account.page.heading.dashboard",
        "language": "default",
        "resource": "account",
        "value": "Dashboard"
    },
    ...
]
```

## Add Translation 

**URL** : `/api/translations/`

**Method** : `POST`

### Success Responses

**Condition** : User can create a new Translation.

**Code** : `201 Created`

```json
{
    "key": "{key}",
    "language": "{language}",
    "resource": "{language}",
    "translation": "{resource}"
}
```

## Update Translation 

**URL** : `/api/translations/{id}`

**Method** : `POST`

### Success Responses

**Condition** : User can update a Translation.

**Code** : `200 OK`

```json
{
    "key": "{key}",
    "language": "{language}",
    "resource": "{language}",
    "translation": "{resource}"
}
```

## Delete Translation 

**URL** : `/api/translations/{id}`

**Method** : `DELETE`

### Success Responses

**Condition** : User can delete a Translation.

**Code** : `204 No Content`

```json
```




# Setup 

You are free, encouraged even, to use any language/technology you are comfortable with. If you have any questions you can always contact us.

## Handing in the assesment 

When you're finished developing the application and did all the git commits, you can zip the entire folder (don't forget the .git), and send it to us in reply of the mail you got with this assessment.

## Good luck

And again if you have any questions, let us know!