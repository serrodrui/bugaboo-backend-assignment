# Bugaboo Backend Assignment 

Fork from: yuriteixeira/pure-php-api-boilerplate

## How to run this?

Insider your working copy (where you cloned this repo), run `php -S localhost:8080 web/index.php`.

## Run with docker

```sh
# Build the image
docker build -t bugaboo-backend-assignment .
# Run the image in a container
docker run -it --rm --name bugaboo-backend-assignment -p 8080:8080 bugaboo-backend-assignment
```

### API DOC

https://documenter.getpostman.com/view/51547/SW18wFMM?version=latest

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/7c91c4e010d656e8aaf9)

## Get All Translations 
```sh
curl -X GET \
  'http://localhost:8080/api/translations' \
  -H 'cache-control: no-cache' | jq '.'
```

## Filter by language 
```sh
curl -X GET \
  'http://localhost:8080/api/translations?lan=zh' \
  -H 'cache-control: no-cache' | jq '.'
```

## Filter by section
```sh
curl -X GET \
  'http://localhost:8080/api/translations?lan=zh&sec=forms' \
  -H 'cache-control: no-cache' | jq '.'
```

## Filter by key
```sh
curl -X GET \
  'http://localhost:8080/api/translations?key=check.gift&lan=zh&sec=forms' \
  -H 'cache-control: no-cache' | jq '.'
```

## Add Translation
```sh
curl -X POST \
  http://localhost:8080/api/translations \
  -d 'key=select.option.country.netherlands&language=zh&resource=forms&translation=nederland' | jq '.'
```

## Update Translation 
```sh
  curl -X PUT \
  http://localhost:8080/api/translations/forms.select.option.country.netherlands \
  -d 'language=zh&translation=The%20Netherlands' | jq '.'
```

## Delete Translation 
```sh
  curl -X DELETE \
  http://localhost:8080/api/translations/forms.select.option.country.netherlands \
  -d 'language=zh' | jq '.'
```