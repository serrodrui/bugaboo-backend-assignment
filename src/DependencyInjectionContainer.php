<?php

namespace PurePhpApi;

use PurePhpApi\Command\GetTranslations;
use PurePhpApi\Command\CreateTranslation;
use PurePhpApi\Command\DeleteTranslations;
use PurePhpApi\Command\UpdateTranslation;

class DependencyInjectionContainer
{
    private $definitions = [];
    private $instances = [];
    private $server = [];
    private $get = [];
    private $post = [];

    public function __construct($server, $get, $post)
    {
        $this->server = $server;
        $this->get = $get;
        $this->post = $post;
        
        $this->definitions['getTranslations'] = function($server, $get, $post) {
            return new GetTranslations($server, $get, $post);
        };

        $this->definitions['createTranslation'] = function($server, $get, $post) {
            return new CreateTranslation($server, $get, $post);
        };

        $this->definitions['deleteTranslations'] = function($server, $get, $post) {
            return new DeleteTranslations($server, $get, $post);
        };

        $this->definitions['updateTranslation'] = function($server, $get, $post) {
            return new UpdateTranslation($server, $get, $post);
        };
    }

    public function get($id)
    {
        if (!isset($this->definitions[$id])) {
            throw new Exception\DependencyNotFound();
        }

        if (!array_key_exists($id, $this->instances)) {
            $this->instances[$id] = $this->definitions[$id](
                $this->server, 
                $this->get, 
                $this->post
            );
        }

        return $this->instances[$id];
    }
}
