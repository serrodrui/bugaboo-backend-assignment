<?php 

namespace PurePhpApi;

class Dispatcher 
{
    private $container;

    public function __construct(DependencyInjectionContainer $container)
    {
        $this->container = $container;
    }

    public function dispatch($uri, $method)
    {
        $prefix = "api";
        $collection = "translations";
        preg_match(
            "/(^\/$prefix\/$collection\/?)(?<document>.*)/",
            parse_url($uri, PHP_URL_PATH),
            $uri_parts
        );
        $document = $uri_parts["document"];

        $map = [
            'GET /api/translations/' => 'getTranslations',
            'POST /api/translations/' => 'createTranslation',
            "DELETE /api/translations/$document" => 'deleteTranslations',
            "PUT /api/translations/$document" => 'updateTranslation'
        ];

        $route = "$method /$prefix/$collection/$document";
        $match = $service = isset($map[$route]) ? $map[$route] : null;
        if (!$match) throw new Exception\NoMatchingRoute();

        $command = $this->container->get($service);
        return $command;
    }
}
