<?php

namespace PurePhpApi\Utils;

class WebHelper 
{
    public static function res($body = [], $status = 200) {
        return array(
            "body" => $body,
            "status" => $status
        );
    }

    public static function put() {
        // Fake $_PUT
        $put  = array();
        parse_str(file_get_contents('php://input'), $put);

        return $put;
    }

    public static function delete() {
        // Fake $_DELETE
        $delete  = array();
        parse_str(file_get_contents('php://input'), $delete);

        return $delete;
    }
}