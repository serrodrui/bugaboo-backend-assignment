<?php

namespace PurePhpApi\Utils;

class CommandHelper
{
    public static function build_resources_regex($lan, $sec) {
        $section_regex_part = isset($sec) && !empty($sec) ? $sec : '[a-zA-Z]+';
        $lan_regex_part = isset($lan) && !empty($lan) ? '_' . $lan . '_[A-Z]{2}' : '';

        return '/^'.$section_regex_part.$lan_regex_part.'.properties$/';
    }

    public static function get_resources($lan, $sec) {   
        $resources = preg_grep(
            CommandHelper::build_resources_regex($lan, $sec), 
            scandir($_SERVER['DOCUMENT_ROOT'] . '/resources')
        );

        return array_values($resources);
    }

    public static function get_resource_name($resource) {
        preg_match('/(?<resourceName>^[a-z]+)/', $resource, $matches);

        return $matches["resourceName"];
    }

    public static function get_translations($resource, $key) {
        $key_regex_part = isset($key) && !empty($key) ? $key : '[a-z]+(\.[a-z0-9]+){0,}';

        preg_match_all(
            '/(^' . $key_regex_part . ')=(.+)/im', 
            CommandHelper::get_resource_content($resource), 
            $groups
        );

        return array_combine($groups[1], end($groups));
    }

    public static function build_translation_item($keys, $translations, $language, $resource) {
        $item = [];
        foreach($keys as $index => $key) {
            $item[] = array(
                "key" => $key,
                "language" => isset($language) && !empty($language) ? $language : 'default',
                "resource" => CommandHelper::get_resource_name($resource),
                "translation" => $translations[$index]
            );
        }

        return count($item) === 1 ? $item[0] : $item;
    }

    public static function decode_id($id) {
        preg_match('/^(?<section>[a-z]+)\.(?<key>.*)/', $id, $matches);

        return $matches;
    }

    public static function get_resource_content($resource) {
        return file_get_contents($_SERVER['DOCUMENT_ROOT']. '/resources/' . $resource);
    }

    public static function update_resource_content($resource, $key, $translation) {
        $resource_content = CommandHelper::get_resource_content($resource);
        $new_resource_content = preg_replace('/(^' . $key . ')=(.+)/im', "$key=$translation", $resource_content);
        $update = $resource_content !== $new_resource_content;

        if ($update)
            file_put_contents($_SERVER['DOCUMENT_ROOT']. '/resources/' . $resource, $new_resource_content);

        return $update;
    }

    public static function delete_resource_content($resource, $regex) {
        $content = preg_replace(
            $regex,
            '',
            CommandHelper::get_resource_content($resource)
        );

        file_put_contents($_SERVER['DOCUMENT_ROOT']. '/resources/' . $resource, $content);
    }

    public static function create_resource_content($resource, $key, $translation) {
        file_put_contents(
            $_SERVER['DOCUMENT_ROOT']. '/resources/' . $resource, 
            "$key=$translation", 
            FILE_APPEND
        );
    }
}
