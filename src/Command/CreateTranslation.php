<?php

namespace PurePhpApi\Command;

use PurePhpApi\Utils\CommandHelper;
use PurePhpApi\Utils\WebHelper;

class CreateTranslation
{

    private $post;

    public function __construct($server, $get, $post)
    {
        $this->post = $post;
    }

    public function execute()
    {
        $language = $this->post["language"];
        $translation = $this->post["translation"];
        $resource = $this->post["resource"];
        $key = $this->post["key"];

        $resources = CommandHelper::get_resources($language, $resource);

        foreach($resources as $resource) {
            if (!empty(CommandHelper::get_translations($resource, $key))) {
                CommandHelper::update_resource_content(
                    $resource,
                    $key,
                    $translation
                );
            } else {
                CommandHelper::create_resource_content(
                    $resource, 
                    $key, 
                    $translation
                );
            }
        }

        $res = CommandHelper::build_translation_item([$key], [$translation], $language, $resource);

        return WebHelper::res($res, 201);
    }
}
