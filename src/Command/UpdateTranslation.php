<?php

namespace PurePhpApi\Command;

use PurePhpApi\Utils\CommandHelper;
use PurePhpApi\Utils\WebHelper;

class UpdateTranslation 
{

    private $document;

    public function __construct($server)
    {
        $this->document = basename($server['REQUEST_URI']);
    }

    public function execute()
    {
        $id = CommandHelper::decode_id($this->document);

        $put  = WebHelper::put();

        $language = $put["language"];
        $translation = $put["translation"];
        $section = $id["section"];
        $key = $id["key"];

        $resources = CommandHelper::get_resources($language, $section);
        
        foreach($resources as $resource) {
            $updated = CommandHelper::update_resource_content(
                $resource,
                $key,
                $translation
            );

            $res = CommandHelper::build_translation_item([$key], [$translation], $language, $resource);
        }

        return WebHelper::res($updated ? $res : [], $updated ? 200 : 409);
    }
}
