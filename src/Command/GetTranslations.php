<?php

namespace PurePhpApi\Command;

use PurePhpApi\Utils\CommandHelper;
use PurePhpApi\Utils\WebHelper;

class GetTranslations
{

    private $get;

    public function __construct($server, $get)
    {
        $this->get = $get;
    }

    public function execute()
    {
        $language = $this->get["lan"];
        $section = $this->get["sec"];
        $key = $this->get["key"];

        $res = [];

        $resources = CommandHelper::get_resources($language, $section);

        foreach($resources as $resource) {
            $translations = CommandHelper::get_translations($resource, $key);

            $res = array_merge(
                $res, CommandHelper::build_translation_item(
                    array_keys($translations), 
                    array_values($translations), 
                    isset($language) && !empty($language) ? $language : 'default', 
                    pathinfo($resource, PATHINFO_FILENAME)
                )
            );
        }
        
        return WebHelper::res($res);
    }
}
