<?php

namespace PurePhpApi\Command;

use PurePhpApi\Utils\CommandHelper;
use PurePhpApi\Utils\WebHelper;

class DeleteTranslations
{

    private $document;

    public function __construct($server)
    {
        $this->document = basename($server['REQUEST_URI']);
    }

    public function execute()
    {
        $id = CommandHelper::decode_id($this->document);

        $delete = WebHelper::delete();

        $language = $delete["language"];
        $section = $id["section"];
        $key = $id["key"];

        $resources = CommandHelper::get_resources($language, $section);
        
        foreach($resources as $resource) {
            CommandHelper::delete_resource_content(
                $resource,
                '/(^' . $key . ')=(.+)/im'
            );
        }

        return WebHelper::res(null, 204);
    }
}
